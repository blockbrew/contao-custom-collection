<?php

namespace Blockbrew\Bundle\ContaoCustomCollectionBundle\Model;

class CustomCollectionArchiveModel extends \Model
{
    protected static $strTable = 'tl_custom_collection_archive';
}
